require('dotenv').config()
const TelegramBot = require('node-telegram-bot-api');

const token = process.env.BOT_TOKEN;
const webAppUrl = 'https://curriculum-mini-app.vercel.app';

const bot = new TelegramBot(token, { polling: true });

bot.onText(/\/echo (.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    const resp = match[1];

    bot.sendMessage(chatId, resp);
});

bot.on('message', async (msg) => {
    const chatId = msg.chat.id;
    const text = msg.text;
    if (text === '/start') {
        await bot.sendMessage(chatId, 'Ниже появится кнопка, открыть КУБ.Расписание', {
            reply_markup: {
                keyboard: [
                    [{ text: 'Открыть приложение' }]
                ]
            }
        })

        await bot.sendMessage(chatId, 'Заходи в КУБ.Расписание', {
            reply_markup: {
                inline_keyboard: [
                    [{ text: 'Открыть КУБ.Расписание', web_app: { url: webAppUrl } }]
                ]
            }
        })
    }

});
